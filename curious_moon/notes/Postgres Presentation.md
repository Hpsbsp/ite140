## History of PostgreSQL
In 1974, at the University of California at Berkeley, two faculty members, Michael Stonebraker and Eugene Wong started researching relational database systems after reading Edgar Frank "Ted" Codd's papers about the Relational Data Model. They called their project “INGRES” which stands for “Interactive Graphics and Retrieval System.” In contrast to IBM’s System R, INGRES used the QUEL language instead of SQL, it ran in UNIX and on less powerful computers. In 1986, Michael Stonebraker released Postgres which was supposed to be a new open source relational database management system that would be a successor to INGRES and feature functionality that other RDBMS lacked such as:
- Object Relational datatypes
- Write-Once media
- massive storage
- inferencing
- fat cursors

In 1994, Berkeley students Andrew Yu and Jolly Chen replaced the QUEL language interpreter in Postgres with one for SEQUEL and released it as Postgres95 version 0.01 on May 1, 1995 and version 1.0 on September 5, 1995. In 1996 Postgres95 was renamed to PostgreSQL and they resumed the version numbering at 6.0 from the original Postgres project from Berkeley. A few of the major advancements in PostgreSQL included the already mentioned SQL interpreter, the psql program which is used for interactive SQL queries, libpgtcl for tcl based clients.

## Michael Stonebraker
Background Info
Michael Ralph Stonebraker was born October 11, 1943, in Newburyport, Massachusetts. His mother was a teacher and his father was an engineer. He went to Governor Dummer Academy instead of a high school because the town didn’t have a high school and would instead pay tuition for students to go to Governor Dummer Academy instead.

He got a bachelor’s degree in electrical engineering in 1965 at Princeton University in New Jersey, U.S.

He got a master’s degree in engineering in 1966 and a Ph.D. in computer information and control engineering in 1971 at the University of Michigan.

He became an assistant professor at the University of California, Berkeley in 1971 where he met Eugene Wong who was Professor of Electrical Engineering and Computer Sciences.

In 1980, Relational Technology Inc. was created by Michael Stonebraker, Eugene Wong and Lawrence A. Rowe who was a professor at the University of California, Berkeley. 

In 1985, INGRES was discontinued and in 1986 POSTGRES started being developed. 

Michael Stonebraker retired from the University of California, Berkeley in 1994 and in 2001 he got a job at MIT.

Since then he has co-founded the following companies: 
Ingres Corporation, Illustra, Paradigm4, StreamBase Systems, Vertica, VoltDB, Informix and Tamr

## Relational databases & Codd

Edgar Frank “Ted” Codd was entirely behind the concept of the Relational Database Model. He was born August 19, 1923, Portland, Dorset, England and died April 18, 2003, Williams Island, Florida, U.S. He worked at IBM between 1949 and 1984. In 1970, after his IBM paper the year before, he released his “A Relational Model of Data for Large Shared Data Banks” paper which explained his idea for a new kind of database called the “Relational Model”. This model was different from the Navigational model used because the Relational Model worked the same way across tools and programs. If a company used the Navigational Model, it would have to hire a specialist for a specific tool because one method wouldn’t work with all databases. With the relational model, a user only needed to know the basic SQL language to manage any Relational database. The Relational Model allowed you to access data with queries rather than following references from other objects in the database to the path of the data. It also offers much better security than the Navigational model. 

## Postgres' place in the database landscape

PostgreSQL is fully ACID-compliant which means that it is useful in the financial industry and ideal for Online Transaction Processing. It can be integrated with Matlab and R. PostgreSQL also features PostGIS which adds support for Geographic Information Systems. PostrgeSQL has many big companies as customers such as Uber, Netflix, IMDB, Instacart, Instagram, Spotify, Twitch and Reddit. 
