# RAID

RAID stands for Redundant Array of Independent Disks. RAID uses multiple disk drives to store its own fraction of the same data and then reads each drive at the same time in order to improve performance and sometimes failure tolerance. Having multiple disks with smaller size data to read is much faster than one high power drive with all of the data on it, and having backups of the data with Parity Data can prevent loss of data even if a disk fails. Norman Ken Ouchi from IBM designed a RAID system in 1974 which would later be called RAID 4 and patented it in 1977. The term RAID was coined in 1987 by David Patterson, Randy Katz and Garth A. Gibson.

Types of RAID:
- RAID 0: Uses a minimum of two disks and evenly splits the data between the disks by splitting data into blocks and writing it simultaneously or sequentially on each of the disks. The purpose of RAID 0 is speed and performance.

- RAID 1: Uses a minimum of two disks in which the same data is stored for redundancy. This is called mirroring. The purpose of RAID 1 is redundancy.

- RAID 2: Uses two sets of disks. The first is for striping and the second is for error checking. This type of RAID is rarely used now.

- RAID 3: Uses a minimum of 3 disks. One disk is used for parity and the rest are for striping.

- RAID 4: Similar to RAID 3 but uses independent disks so it doesn't need a controller.

- RAID 5: Uses a minumum of 3 disks and a maximum of 16. It combines striping with a parity by splitting the parity evenly across the disks. If one disk fails, the parities ensures that data is not lost. This type of RAID is fast and can tolerate a single drive failure.

- RAID 6: Similar to RAID 5 but uses 2 parities which are split between all of the disks evenly. Requires a minimum of 4 disks. This allows it to compensate for a second drive failure.

- RAID 10: Also known as RAID 1+0, this RAID type combines RAID 1 and RAID 0. Requires a minimum of 4 disks and a controller. There are two groups of disks which use mirroring between the disks in their group, meanwhile there is striping between the two disk groups. 

# SQL Server

An SQL Server (or it can go by as RDBMS) is a certain relational database management system that was developed first by microsoft in 1988. SQL Servers are mainly made for managing and storing information and are as well designed to compete with mySQL and Oracle databases meaning they’re in a competition to see which database management system is the best. 

In 1988 microsoft was in the process of developing the first database, it was published in 1989. Microsoft’s goal in creating this SQL Server was to develop database creations and maintenance software that could provide some significant information/data to Microsoft’s business database market.

# SSD

SSD, other known as Solid State Drives. Solid State Drives are types of storage devices used on computers which I believe some of you may already know; stores significant data on solid-state flash memory. 

There as well are HDDs also known as Hard Disk Drives. A significant difference in those following two drives is that as Hard Disk Drives are storage devices that read and write data, Solid State Drives use a newer technology that stores its data instantly making them considered faster, quieter, and more durable. 

The first SSD was developed over 70 years ago and most of them seemed to have two similar technologies known as a magnetic core memory and a card capacitor read-only store (also known as CCROS). 
